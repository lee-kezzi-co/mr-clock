//
//  Clock.swift
//  clock
//
//  Created by Lee Irvine on 3/19/20.
//  Copyright © 2020 Kezzi.co. All rights reserved.
//

import Foundation
import Combine

class Clock: ObservableObject {
    var didChange = PassthroughSubject<Void, Never>()

    private var ticker: Timer!
    
    var time: Double = 0.0 {
        didSet {
            self.didChange.send()
//            self.ticker.invalidate()
        }
    }
    
    init() {
        self.ticker = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: self.onTick)
    }
    
    func reset() {
        
    }
    
//    private func tick() {
//        self.ticker = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: self.onTick)
//    }
    
    private func onTick(_ : Timer) {
        let hour = Double(Calendar.current.component(.hour, from: Date()))
        
        let minute = Double(Calendar.current.component(.minute, from: Date()))
        
        self.time = hour + (minute / 100.0)
    }
}
