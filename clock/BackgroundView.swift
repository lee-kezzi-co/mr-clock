//
//  BackgroundView.swift
//  clock
//
//  Created by Lee Irvine on 3/27/20.
//  Copyright © 2020 Kezzi.co. All rights reserved.
//

import SwiftUI

extension View {
    func diameter(_ g: GeometryProxy) -> CGFloat {
        return min(g.size.width, g.size.height)
    }
}

struct BackgroundView: View {
    @Binding var time: Double
    
    var body: some View {
        GeometryReader { g in
            ZStack {
                Image("sun").resizable()
                    .frame(width: self.diameter(g) * 0.33, height: self.diameter(g) * 0.33, alignment: .center)
                    .offset(x: 0, y: self.diameter(g) * -0.5)
                    .rotationEffect(self.sunRotation())
                
                Image("moon").resizable().frame(width: self.diameter(g) * 0.22, height: self.diameter(g) * 0.22, alignment: .center)
                    .offset(x: 0, y: self.diameter(g) * -0.5)
                    .rotationEffect(self.moonRotation())
                
                Image("background-city").resizable().frame(
                    width: self.diameter(g) * self.cityAspect(), height: self.diameter(g))
                    .offset(x: 0, y: 0.0)

//                Rectangle()
//                    .foregroundColor(Color.green)
//                    .frame(width: g.size.width, height: g.size.height * 0.5, alignment: .center)
//                    .offset(x: 0, y: g.size.height / 2.0)
//
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
            .background(self.backgroundColor())
        }
    }

    private func cityAspect() -> CGFloat { return 6524.0 / 1867.0 }

    private func backgroundColor() -> Color {
        let l = sin( self.time * Double.pi / 24.0)
        
        return Color(hue: 222.0 / 360.0, saturation: 1.0, brightness: 1.0 * l)
    }
    
    private func sunRotation() -> Angle {
        let a = self.time / 24.0 * 360.0
        
        return Angle(degrees: a + 180.0)
    }

    private func moonRotation() -> Angle {
        let a = self.time / 24.0 * 360.0
        
        return Angle(degrees: a + 0.0)
    }

}

struct BackgroundView_Previews: PreviewProvider {
    static var previews: some View {
        let hour = Double(Calendar.current.component(.hour, from: Date()))
        
        let minute = Double(Calendar.current.component(.minute, from: Date()))
        
        let time = hour + (minute / 100.0)

        return BackgroundView(time: .constant(time))
    }
}
