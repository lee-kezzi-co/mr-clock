//
//  ClockView.swift
//  clock
//
//  Created by Lee Irvine on 3/21/20.
//  Copyright © 2020 Kezzi.co. All rights reserved.
//

import SwiftUI
import AVFoundation

struct ClockView: View {
    
    @EnvironmentObject var speaker: ClockSpeaker
    
    @Binding var time: Double {
        didSet {
            if self.time > 24.0 {
                self.time -= 24.0
            }
            
            if self.time < 0.0 {
                self.time += 24.0
            }
        }
    }
    
    var body: some View {
        GeometryReader { g in
            ZStack {
                // clock border
                Circle()
                    .stroke(lineWidth: self.diameter(g) * 0.08)
                    .fill(Color.yellow)
                    .frame(minWidth: 0.0, maxWidth: .infinity, minHeight: 0.0, maxHeight: .infinity)
                
                // clock background
                Circle()
                    .fill(Color.blue)
                    
                // clock numbers
                ForEach(0..<12) { i in
                    self.ClockNumber(i, g)
                }
                                
                // minute hand
                Rectangle()
                    .frame(width: self.minuteHandWidth(g), height: self.minuteHandHeight(g))
                    .offset(x:0, y: self.minuteHandHeight(g) * 0.5)
                    .foregroundColor(Color.red)
                    .rotationEffect(self.angleForMinute(self.time))
                    .gesture(self.dragMinuteGesture(g))

                // hour hand
                Rectangle()
                    .frame(width: self.hourHandWidth(g), height: self.hourHandHeight(g))
                    .offset(x:0, y: self.hourHandHeight(g) * 0.5)
                    .foregroundColor(Color.white)
                    .rotationEffect(self.angleForHour(self.time))
                    .gesture(self.dragHourGesture(g))

            }
            .frame(width: self.diameter(g), height: self.diameter(g), alignment: .center)
        }
    }

    private func dragMinuteGesture(_ g: GeometryProxy) -> some Gesture {
        DragGesture()
        .onChanged { ges in
            let lastMinute = Double(Int(self.time * 100) % 100)/100.0
            
            let p = CGPoint(
                x: ges.location.x - self.minuteHandWidth(g)/2.0,
                y: ges.location.y - self.minuteHandHeight(g)/2.0)

            let gestureAngle = Angle(radians: Double(atan2(-p.x, p.y)) )

            let minuteForAngle = (gestureAngle.degrees + 180.0) / 360.0
            
            var delta = minuteForAngle - lastMinute
            
            if delta >= 0.9 {
                delta = -0.01
            }

            if delta <= -0.9 {
                delta = 0.01
            }
            
            self.time += delta
        }
        .onEnded { _ in
            self.speaker.say(time: self.time)
        }
    }
    
    private func dragHourGesture(_ g: GeometryProxy) -> some Gesture {
        DragGesture()
        .onChanged { ges in
            let p = CGPoint(
                x: ges.location.x - self.hourHandWidth(g)/2.0,
                y: ges.location.y - self.hourHandHeight(g)/2.0)
            
            let gestureAngle = Angle(radians: Double(atan2(-p.x, p.y)) )

            let hourForAngle = (gestureAngle.degrees + 180.0) / 360.0 * 12.0

//            var delta = hourForAngle - self.time
            var delta = hourForAngle - (self.time > 12.0 ? self.time - 12.0 : self.time)
            
            if delta >= 11.0 {
                delta = -0.1
            }

            if delta <= -11.0 {
                delta = 0.1
            }
            
            self.time += delta
        }
        .onEnded { _ in
            self.speaker.say(time: self.time)
        }

    }

    private func hourHandWidth(_ g: GeometryProxy) -> CGFloat {
        return diameter(g) * 0.07
    }

    private func hourHandHeight(_ g: GeometryProxy) -> CGFloat {
        return diameter(g) / 5.0
    }

    private func minuteHandWidth(_ g: GeometryProxy) -> CGFloat {
        return diameter(g) * 0.075
    }
    
    private func minuteHandHeight(_ g: GeometryProxy) -> CGFloat {
        return diameter(g) / 3.0
    }
    
    private func angleForHour(_ hour: Double) -> Angle {
        let offset = 180.0
        
        let amountToRotatePerHour = 360.0 / 12.0
        
        let degrees = Double(hour) * amountToRotatePerHour

        return Angle(degrees: degrees + offset)
    }

    // minute: number from 0 to 1.0
    private func angleForMinute(_ minute: Double) -> Angle {
        let offset = -180.0
        
        let amountToRotatePerMinute = 360.0
        
        let degrees = Double(minute) * amountToRotatePerMinute
        
        let angle = Angle(degrees: degrees + offset)
        
        return angle
    }
    
    func ClockNumber(_ digit: Int, _ g: GeometryProxy) -> some View {
        ZStack {
            Text("\(digit == 0 ? 12 : digit)")
                .foregroundColor(Color.white)
                .fontWeight(.bold)
                .font(.system(size: self.diameter(g)  * 0.07))
        }
        .rotationEffect(self.angleForHour(Double(-digit)))
        .offset(x: 0, y: self.diameter(g) / 2.45)
        .frame(width: self.diameter(g) * 0.12, height: self.diameter(g) / 2.0)
        .rotationEffect(self.angleForHour(Double(digit)))
        .gesture(TapGesture(count: 1)
        .onEnded {
            self.time = Double(digit);
            self.speaker.say(time: self.time)
        })

    }

}

struct ClockView_Previews: PreviewProvider {
    static var previews: some View {
        let hour = Double(Calendar.current.component(.hour, from: Date()))
        
        let minute = Double(Calendar.current.component(.minute, from: Date()))
        
        let time = hour + (minute / 100.0)

        return ClockView(time: .constant(time))
    }
}
