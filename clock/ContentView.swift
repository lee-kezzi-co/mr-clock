//
//  ContentView.swift
//  clock
//
//  Created by Lee Irvine on 3/18/20.
//  Copyright © 2020 Kezzi.co. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    @State var time: Double = 7.00
    
    var body: some View {
        ZStack {
            BackgroundView(time: self.$time)
            
            GeometryReader { g in
                ZStack {
                    ZStack {
                        Text("\(self.formatTime())")
                            .foregroundColor(Color.white)
                            .font(Font.custom("Digital-7", size: self.diameter(g) * 0.11))
                            .multilineTextAlignment(.center)
                            .padding(EdgeInsets(top: 11.0, leading: 22.0, bottom: 11.0, trailing: 22.0))
                    }
                    .background(Color.black)
                    .cornerRadius(10.0)
                    .frame(alignment: .center)
                    .offset(x: 0, y: self.diameter(g) * 0.45)

                    ClockView(time: self.$time)
                        .frame(width: self.diameter(g) * 0.66, height: self.diameter(g) * 0.66, alignment: .center)
                    
                    
                }
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
    }
    
    func formatTime() -> String {
        let ampm = time < 12.0 ? "am" : "pm"
        let hour = (Int(time) % 12) == 0 ? 12 : (Int(time) % 12)

        let minute = Int(Double(Int(time * 100) % 100) / 100.0 * 60.0)

        var formatted = ""
        
        if minute < 10 {
            formatted = "\(hour):0\(minute)\(ampm)"
        } else {
            formatted = "\(hour):\(minute)\(ampm)"
        }
        
        return formatted
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
