//
//  ClockSpeaker.swift
//  clock
//
//  Created by Lee Irvine on 3/21/20.
//  Copyright © 2020 Kezzi.co. All rights reserved.
//

import AVFoundation

class ClockSpeaker: ObservableObject {

    private let synthesizer = AVSpeechSynthesizer()
    
    func say(time: Double) {
        var spokenTime = ""

        let hour = (Int(time) % 12) == 0 ? 12 : (Int(time) % 12)

        let minute = Int(Double(Int(time * 100) % 100) / 100.0 * 60.0)

        if minute == 0 {
           spokenTime = "\(hour) o clock"
        } else if minute < 10 {
            spokenTime = "\(hour) o \(minute)"
        } else {
           spokenTime = "\(hour) \(minute)"
        }

        let utterance = AVSpeechUtterance(string: "\(spokenTime)")

        utterance.voice = AVSpeechSynthesisVoice(language: "en-GB")
        utterance.voice = AVSpeechSynthesisVoice(identifier: "com.apple.speech.synthesis.voice.Fred")
                
        synthesizer.speak(utterance)
    }
    
    

}
